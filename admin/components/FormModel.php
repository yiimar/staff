<?php

namespace admin\components;

/**
 * Description of Model class
 *
 * @category Component
 * @package  backend.modules.staff.components
 * @author   AKulikov <im@kulikov.im>
 * @version  0.1
 */

use \Yii;
use \CActiveForm;

class FormModel extends \CFormModel
{
    /**
     * Fill form fields:
     * 
     * @return mixed, request data
     */
    public function populate()
    {
        $class = str_replace('\\', '_', get_class($this));

        $data = Yii::app()->getRequest()->getPost($class);

        $this->setAttributes($data);

        return $data;
    }

    /**
     * AJAX validation:
     * 
     * @param string $ajaxValue - request value name
     * 
     * @return void
     */
    public function ajaxValidate($ajaxValue = null)
    {
        if (Yii::app()->getRequest()->getPost('ajax') === $ajaxValue) {
            echo CActiveForm::validate($this);
            
            Yii::app()->end();
        }
    }

    /**
     * Получаем placeholder:
     * 
     * @param string $field - field name
     * 
     * @return string, field placeholder
     */
    public function getPlaceHolder($field)
    {
        return Yii::t('staff', 'Input :field_name', [
            ':field_name' => mb_strtolower($this->getAttributeLabel($field), 'UTF-8'),
        ]);
    }
}