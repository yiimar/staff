<?php

namespace admin\components;

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class AdminController extends \CController
{
    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
     public $layout      = '//layouts/admin2';
    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu        = [];
    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = [];
    /**
     * @var type string 
     */
    public $pageTitle   = 'Staff';    
    /**
     * Base model name for CRUD
     * @var type string
     */   
    public $modelClass;

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Group the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
    	$model = \CActiveRecord::model($this->modelClass)->findByPk($id);
        
    	if($model===null)           throw new CHttpException(404,'The requested page does not exist.');
        else                        return $model;
    }
}