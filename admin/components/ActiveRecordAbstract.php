<?php

namespace admin\components;

use master\components\helpers as nsComponentsHelpers;

/**
 * Class ActiveRecordAbstract for advanced usage with CActiveRecord models
 *
 * Special class which gives possibility to log data and some special features.
 * Every ActiveRecord model must extend this class. After you must implement
 * {@link ActiveRecordAbstract::fieldsToLog()} method.
 * For special logging types you model also must implement some interfaces.
 * Please see {@see Logger} and {@see RelationRulesInterface} for additional help
 *
 * @see        Logger
 * @author     aslikeoyu <pekhota.alex@gmail.com>
 * @version    1.0
 * @package    database
 * @subpackage activeRecord
 *
 */
abstract class ActiveRecordAbstract extends \admin\components\ActiveRecord {

    const R_MODEL                    = 'model';
    const R_VALUE_FIELD              = 'valueField';
    const R_VALUE_FIELD_CALLBACK     = 'valueFieldCallback';
    const R_DEPENDENCY               = 'dependency';
    const R_LABEL_TEMPLATE           = 'labelTemplate';
    const R_LABEL_TEMPLATE_CALLBACK  = 'labelTemplateCallback';
    const R_IGNORE_BY_VALUE          = 'ignoreByValue';

    /**
     * Set save method whether to log or not model operations
     *
     * @see ActiveRecordAbstract::setToLog()
     * @see ActiveRecordAbstract::setNotToLog()
     * @access protected
     * @var bool
     */
    protected static $toLog = true;


    public static function transaction($callback) {
        nsComponentsHelpers\ActiveRecordHelper::transaction($callback);
    }
    /**
     * Always return a model with values
     *
     * Return a model for your criteria.
     * First it goes to db and try to find record
     * If record does not exist it creates new one
     * All the passed parameters will be set to record and saved to db
     * You will take clear model from db
     *
     * @param mixed $condition query condition or criteria.
     * @param array $params    parameters to be bound to an SQL statement.
     * @param array $values    values to set for model
     *
     * @return \CActiveRecord | ActiveRecordAbstract
     * @throws \CHttpException
     */
    public static function getModelWithValues(
        $condition = '',
        array $params = array(),
        array $values = array()
    ) {
        $model = static::model()->find($condition, $params);

        if ($model === null) {
            $modelName = get_called_class();
            $model     = new $modelName();
        }

        foreach ($values as $name => $value) {
            if ($model->getAttribute($name) != $value) {
                $model->setAttribute($name, $value);
            }
        }

        if (!$model->save()) {
            throw new \CHttpException(500, nsComponentsHelpers\ActiveRecordHelper::getFirstModelError($model));
        }

        return $model;
    }

    protected static function getTableName($tableName) {
        return nsComponentsHelpers\ActiveRecordHelper::getTableName(true, $tableName);
    }

    public function setToLog() {
        self::$toLog = true;
    }

    public function setNotToLog() {
        self::$toLog = false;
    }

    /**
     * @param bool $runValidation
     * @param null $attributes
     *
     * @return bool
     * @throws \CDbException|\Exception
     */
    public function save($runValidation = true, $attributes = null) {
        try {
            return parent::save($runValidation, $attributes);
        } catch (\CDbException $e) {
            $exceptionHandler = new \DbExceptionHandler($e);
            $method           = $exceptionHandler->getHandlerMethodName();
            if ($method && method_exists($this, $method)) {
                $this->$method($exceptionHandler->arg());
            } else {
                throw $e;
            }
        }
    }

    /**
     * Removes all script tags from text fields
     *
     * @return bool
     */
    protected function beforeSave() {
        if (!parent::beforeSave()) {
            return false;
        }

        $attributes = $this->getAttributes();

        foreach ($attributes as $field => $value) {
            if (is_string($value)) {
                $pattern = "!<script[^>]*>(.)*</script>!Uis";
                $value   = preg_replace($pattern, '', $value);
                $this->setAttribute($field, $value);
            }
        }

        return true;
    }

    public function getFirstError() {
        nsComponentsHelpers\ActiveRecordHelper::getFirstError($this);
    }

    public static function loadByPk($id) {
        $className = get_called_class();
        $model=$className::model()->findByPk($id);
        if ($model===null) {
            throw new \CHttpException(404, 'The requested model('.$className.') does not exist.');
        }
        return $model;
    }

}