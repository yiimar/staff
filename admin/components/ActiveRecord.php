<?php

namespace admin\components;

class ActiveRecord extends \CActiveRecord {
    /**
     * Stores data after finding it from DB
     * For get data use getAfterFindAttributes
     *
     * @see ActiveRecordAbstract::afterFind()
     * @access protected
     * @var array
     */
    protected $afterFindAttributes = array();

    protected function afterFind() {
        parent::afterFind();
        $this->afterFindAttributes = $this->getAttributes();
    }

    /**
     * @param null $name
     *
     * @return array|bool
     */
    public function getAfterFindAttributes($name = null) {
        if ($name === null) {
            return $this->afterFindAttributes;
        }

        if (array_key_exists($name, $this->afterFindAttributes)) {
            return $this->afterFindAttributes[$name];
        }

        return false;
    }

    public function hasChanged($field) {
        if ($this->isNewRecord) {
            return false;
        }

        $wasField = $this->getAfterFindAttributes($field);
        $becomeField = $this->getAttribute($field);
        if ($wasField !== $becomeField) {
            return true;
        }

        return false;
    }
}