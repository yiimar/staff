<?php

namespace admin\components;

use \Yii;

class WebModule extends \CWebModule
{
    public function init()
    {
	// import the module-level models, components and controllers
	$this->setImport(
                [
                    $this->name.'.forms.*',
                    $this->name.'.models.*',                    
                    $this->name.'.components.*',
                    $this->name.'.controllers.*',
                ]
	);
    }

    public function beforeControllerAction($controller, $action)
    {
    	if(parent::beforeControllerAction($controller, $action))        return true;
        else                                    			return false;
    }
}
