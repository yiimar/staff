<?php
/**
 * index.php
 *
 * @author: antonio ramirez <antonio@clevertech.biz>
 * Date: 7/22/12
 * Time: 11:13 AM
 */
date_default_timezone_set('Europe/Kiev');

$isProduction = (int)'%IS_PRODUCTION%';

if ($isProduction) {
    error_reporting(E_ALL);

    ini_set('display_errors', 'Off');
    ini_set('display_startup_errors', 'Off');

    defined('YII_DEBUG') or define('YII_DEBUG', false);
    defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 0);
} else {
    error_reporting(E_ALL);

    ini_set('display_errors', 'On');
    ini_set('display_startup_errors', 'On');

    defined('YII_DEBUG') or define('YII_DEBUG', true);
    defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);
}

chdir(dirname(__FILE__).'/../..');
require_once('%MASTER_APP_PATH%/vendor/autoload.php');

$yii='%MASTER_APP_PATH%/vendor/yiisoft/yii/framework/yii.php';
$config='%ADMIN_PATH%/backend/config/main.php';

require_once($yii);
Yii::createWebApplication($config)->run();
