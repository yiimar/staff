<?php
/**
 * index.php
 *
 * @author: yiimar
 * Date: 7/22/14
 * Time: 11:13 AM
 */
        
require dirname(__FILE__) . '/../../common/config/definitions.php';

require(VENDOR_PATH . DS . 'autoload.php');
require(YII_PATH    . DS . 'yii.php');

$config = ADMIN_PATH . DS . 'config' . DS . 'main.php';
Yii::createWebApplication($config)->run();
