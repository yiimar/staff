<?php

define ('DS', DIRECTORY_SEPARATOR);
define ('NS',  '\\');

$dir = dirname(__FILE__);

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return [

    'aliases' => require_once $dir . DS . 'aliases.php',
    'params'  => require_once $dir . DS . 'params.php',
    'import'  => require_once $dir . DS . 'import.php',
    'modules' => [],
    
	'basePath'  => $dir . DS . '..',
	'name'      => 'My Console Application',

	// preloading 'log' component
	'preload'   => ['log'],
    
        

	// application components
	'components'=> [
            'db'        => require_once $dir . DS . 'db.php',
		
            'log' => [
                'class'  => 'CLogRouter',
                'routes' => [
                    [
                        'class'  => 'CFileLogRoute',
                        'levels' => 'error, warning',
                    ],
		],
            ],
        ],
];