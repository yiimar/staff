<?php

return [
    'application.models.*',
    'application.components.*',
    'application.controllers.*',
    
    'smtpmail.*',
];
