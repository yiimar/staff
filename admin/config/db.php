<?php

return [
    'connectionString'   => 'mysql:host=localhost;dbname=notus_stage',
    'emulatePrepare'     => true,
    'username'           => 'root',
    'password'           => 'mysql',
    'charset'            => 'utf8',
    'enableProfiling'    => true,
    'enableParamLogging' => true,
    'tablePrefix'        => 'avl_',    
];