<?php

return [
    'user' => [
        'class' => '\admin\modules\user\UserModule',
    ],
    'staff' => [
        'class' => '\admin\modules\staff\StaffModule',
//        'routes' => [
//                'staff' => '/staff/user/index',
//        ],
    ],
    'gii' => [
        'class' => 'system.gii.GiiModule',
        'password' => '111',
        // If removed, Gii defaults to localhost only. Edit carefully to taste.
        'ipFilters' => ['127.0.0.1', '::1'],
    ],
];
