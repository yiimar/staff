<?php

$dir = dirname(__FILE__);

return [

    'aliases'    => require $dir . DS . 'aliases.php',
    'params'     => require $dir . DS . 'params.php',
    'import'     => require $dir . DS . 'import.php',
    'modules'    => require $dir . DS . 'modules.php',
    'components' => require $dir . DS . 'components.php',
    
    'basePath'   => $dir . DS . '..',  
    
    'name'       => 'Staff',
    
    'preload'    => [
        'log',
        'bootstrap',
    ],
    
    'sourceLanguage' => 'en',
    'language'       => 'ru',
    
    'defaultController' => 'base',
    
];