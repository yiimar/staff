<?php

return [
    [
        'label' => Yii::t('admin', 'Home'),
        'url'   => ['/base/index'],
    ],
    [
        'label' => Yii::t('admin', 'Товары'),
        'items' => [
            [
                'label' => Yii::t('admin', 'ЯндексМаркет'),
                'url'   => ['/staff/user/index'],
            ],
            [
                'label' => Yii::t('admin', 'Характеристики'),
                'url'   => ['/staff/group/index'],
            ],
            [
                'label' => Yii::t('admin', 'Продукция'),
                'url'   => ['/staff/module/index'],
            ],
            [
                'label' => Yii::t('admin', 'Рекомендации'),
                'url'   => ['/staff/module/index'],
            ],
        ],
    ],
    [
        'label' => Yii::t('admin', 'SЕО'),
        'items' => [
            [
                'label' => Yii::t('admin', 'Параметры оплаты'),
                'url'   => ['/staff/user/index'],
            ],
            [
                'label' => Yii::t('admin', 'Параметры страниц'),
                'url'   => ['/staff/group/index'],
            ],
            [
                'label' => Yii::t('admin', 'Параметры категорий'),
                'url'   => ['/staff/module/index'],
            ],
        ],
    ],
    [
        'label' => Yii::t('admin', 'Выгрузка для площадок'),
        'items' => [
            [
                'label' => Yii::t('admin', 'Adwords'),
                'url'   => ['/staff/user/index'],
            ],
            [
                'label' => Yii::t('admin', 'Hotline'),
                'url'   => ['/staff/group/index'],
            ],
            [
                'label' => Yii::t('admin', 'Nadavi'),
                'url'   => ['/staff/module/index'],
            ],
            [
                'label' => Yii::t('admin', 'Price'),
                'url'   => ['/staff/user/index'],
            ],
            [
                'label' => Yii::t('admin', 'Yandex Market'),
                'url'   => ['/staff/group/index'],
            ],
            [
                'label' => Yii::t('admin', 'Vcene'),
                'url'   => ['/staff/module/index'],
            ],
        ],
    ],
    [
        'label' => Yii::t('admin', 'News'),
        'items' => [
            [
                'label' => Yii::t('admin', 'News List'),
                'url'   => ['/staff/user/index'],
            ],
        ],
    ],
    [
        'label' => Yii::t('admin', 'Share'),
        'items' => [
            [
                'label' => Yii::t('admin', 'Share List'),
                'url'   => ['/staff/user/index'],
            ],
        ],
    ],
    [
        'label' => Yii::t('admin', 'Staff'),
        'items' => [
            [
                'label' => Yii::t('admin', 'User'),
                'url'   => ['/staff/user/index'],
            ],
            [
                'label' => Yii::t('admin', 'Group'),
                'url'   => ['/staff/group/index'],
            ],
            [
                'label' => Yii::t('admin', 'Module'),
                'url'   => ['/staff/module/index'],
            ],
        ],
    ],
];