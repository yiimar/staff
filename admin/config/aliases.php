<?php

return [
    'admin'    => realpath(ADMIN_PATH),    
    
/////////////  Extensions  \\\\\\\\\\\\\\\\\\\\\\\\\    
    /**
     * 'bootstrap' - YiiBooster-3.1
     */
//    'bootstrap' => realpath(dirname(__FILE__) . DS . '..' . DS . 'extensions' . DS . 'booster'),    
    'bootstrap'  => 'ext.booster',
    'smtpmail'   => 'ext.smtpmail',
];
