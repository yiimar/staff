<?php

date_default_timezone_set('Europe/Kiev');

$isProduction = (int)'0'; //%IS_PRODUCTION%

if ($isProduction) {
    error_reporting(E_ALL);

    ini_set('display_errors',         'Off');
    ini_set('display_startup_errors', 'Off');

    defined('YII_DEBUG')       or define('YII_DEBUG', false);
    defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 0);
} else {
    error_reporting(E_ALL);

    ini_set('display_errors',         'On');
    ini_set('display_startup_errors', 'On');

    defined('YII_DEBUG')       or define('YII_DEBUG', true);
    defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);
}


define ('DS', DIRECTORY_SEPARATOR);
define ('NS',  '\\');


define('APP_PATH',    realpath(dirname(__FILE__) . DS . '..' . DS . '..'));

    define('ADMIN_PATH',    realpath(APP_PATH . DS . 'admin'));
    define('BACKEND_PATH',  realpath(APP_PATH . DS . 'backend'));
    define('FRONTEND_PATH', realpath(APP_PATH . DS . 'frontend'));
    define('COMMON_PATH',   realpath(APP_PATH . DS . 'common'));
    define('MASTER_PATH',   realpath(APP_PATH . DS . 'master'));
    
        define('VENDOR_PATH', realpath(MASTER_PATH . DS . 'vendor'));
