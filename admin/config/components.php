<?php

return [
    'bootstrap' => [
        'class'  => 'bootstrap.components.Bootstrap',
    ],
    
    'user' => [
        // enable cookie-based authentication
//        'class'          => 'WebUser',
        'allowAutoLogin' => true,
        'loginUrl'       => 'staff/user/login',
    ],
    
    'mailer' => [
        'class'     => 'ext.smtpmail.PHPMailer',
        'Charset'   => 'UTF-8',
        'Host'      => 'smtp.yandex.ru',
        'Port'      => 465,
        'SMTPSecure'=> 'SSL',
        'Username'  => 'notus-adm',
        'Password'  => 'nb2014',
        'SMTPAuth'  => true,
    ],
    
    'urlManager' => [
        'urlFormat'      => 'path',
        'showScriptName' => false,
        'urlSuffix'      => '/',
        'rules'          => require $dir . DS . 'rules.php',
    ],
    
    'db' => require $dir . DS . 'db.php',
    
    'errorHandler' => [
        // use 'site/error' action to display errors
//        'errorAction' => 'site/error',
    ],
    
    'log' => [
        'class'  => 'CLogRouter',
        'routes' => [
            [
                'class'   => 'CFileLogRoute',
                'levels'  => 'error, warning',
                'logFile' => 'errors.log',
                'except'  => 'exception.CHttpException.*',
                'logPath' => ADMIN_PATH . DS . 'logs',
            ],
        ],
    ],
];
