<?php

use \Yii;

class BaseController extends \admin\components\AdminController
{
    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()
    {
	$this->render('index');
    }
        
    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
	if($error=Yii::app()->errorHandler->error)
            if(Yii::app()->request->isAjaxRequest)		echo $error['message'];
            else                				$this->render('error', $error);
    }

    /**
     * Displays the login page
     * 
     * @return void
     */
    public function actionLogin()
    {
        $this->render('login', ['model' => new admin\modules\staff\forms\LoginForm,]);
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout()
    {
	Yii::app()->user->logout();
        
	$this->redirect(Yii::app()->homeUrl);
    }
}