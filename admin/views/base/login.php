<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
    'id'                    => 'verticalForm',
//    'enableAjaxValidation'  =>true,
//    'enableClientValidation'=>true,
//    'clientOptions'         => [
//        'validateOnSubmit' =>true,
//        'validateOnChange' =>true,
//        'validateOnType'   =>true,
//    ],    
    'htmlOptions' => ['class' => 'well'], // for inset effect
]);
 
echo $form->textFieldRow($model,     'username',    []);
echo $form->textFieldRow($model, 'password', ['class' => 'span3']);     //passwordFieldRow
echo $form->checkboxRow($model,     'rememberMe');

echo "<br>";
$mm = \admin\modules\staff\models\User::byLogin($model->username);
if ($mm->password == $model->password)   echo "good";
else                                     echo "error";
echo "<br>";

$this->widget('bootstrap.widgets.TbButton', [
    'buttonType' => 'submit',
    'label'      => Yii::t('staff', 'Login'),
]);
 
$this->endWidget();
unset($form);
