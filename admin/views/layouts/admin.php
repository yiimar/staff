<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        
        <title>
            <?php echo (
                isset($this->title) && !empty($this->title)
                    ? CHtml::encode($this->title) . ' - '
                    : ''
            ) . Yii::app()->name; ?>
        </title>
        
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta "http-equiv"="X-UA-Compatible" content="IE=edge">
        
        <base href="<?php echo Yii::app()->createAbsoluteUrl('/'); ?>/">

        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        
    </head>

    <body>
        <?php $this->renderPartial('//layouts/_header'); ?>

        <?php echo $content; ?>

        <?php $this->renderPartial('//layouts/_footer'); ?>
    </body>
</html>
