<div id="header">
    <?php $this->widget('bootstrap.widgets.TbNavbar', [
        'type'      => 'inverse',
        'brand'     => CHtml::encode(Yii::app()->name),
        'brandUrl'  => '/',
        'collapse'  => true,
        'fixed'     => false,
        'fluid'     => true,
        'items'     => [
            [
                'class'       => 'booster.widgets.TbMenu',
                'type'        => 'navbar',
                'htmlOptions' => ['class'=>'pull-right'],
                'items'       => [
                    [
                        'label'   => 'Login',
                        'url'     => ['/staff/user/login'],
                        'visible' => Yii::app()->user->isGuest,
                    ],
                    [
                        'label'   => 'Logout (' . Yii::app()->user->name . ')',
                        'url'     => ['/staff/user/logout'],
                        'visible' => !Yii::app()->user->isGuest,
                    ],
                ],
            ],
        ],
    ]); ?> 
</div><!-- header -->