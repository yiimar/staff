<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/admin'); ?>

<div class="container" style="margin-bottom:80px;">
    <div class="row">
        <div class="col-md-3 well pull-left">
            <?php $this->beginWidget('zii.widgets.CPortlet', ['title' => Yii::t('admin', 'Modules'),]); ?>
            
                <?php $this->widget('bootstrap.widgets.TbMenu', [
                    'type'        => 'tabs',
                    'stacked'     => true,
                    'dropup'      => false,
                    'items'       => require ADMIN_PATH . DS . 'config' . DS . 'menu-admin.php',
//                    'htmlOptions' => ['class' => 'well'],
		]); ?>
            
            <?php $this->endWidget(); ?>
        </div>

        <div class="col-md-9 pull-left" style="width: 680px; margin-left: 20px;">
                <?php echo $content; ?>
        </div>
<!--        <div class="clear"></div>-->
    </div>
</div>      
<?php $this->endContent(); ?>