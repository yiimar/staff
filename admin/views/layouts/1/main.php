<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />

        <!-- blueprint CSS framework -->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
        <!--[if lt IE 8]>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
        <![endif]-->

        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />

        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    </head>

    <body>

        <div class="container" id="page">

            <div id="header">
                <div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
            </div><!-- header -->

            <div id="mainmenu">
                <?php $user = Yii::app()->getModule('user');?>
                <?php $this->widget('zii.widgets.CMenu', [
                    'items' => [
                        [
                            'label' => 'Home',
                            'url'   => ['/site/index']
                        ],
                        [
                            'label' => 'User',
                            'url'   => ['/staff/user/index']
                        ],
                        [
                            'label' => 'Group',
                            'url'   => ['/staff/group/index']
                        ],
                        [
                            'label' => 'Module',
                            'url'   => ['/staff/module/index']
                        ],
                        [
                            'label' => 'Permit',
                            'url'   => ['/staff/permit/index']
                        ],
                        [
                            'label'   => Yii::t('user', "Login"),
                            'url'     => ['/staff/user/login'],//Yii::app()->getModule('user')->loginUrl
                            'visible' => Yii::app()->user->isGuest
                        ],
                        [
                            'label'   => Yii::t('user', "Register"),
                            'url'     => ['/user/registration/registration'],//Yii::app()->getModule('user')->registrationUrl
                            'visible' => Yii::app()->user->isGuest,
                        ],
                        [
                            'label'   => Yii::t('user', "Logout").' ('.Yii::app()->user->name.')',
                            'url'     => ['/staff/user/logout'],//Yii::app()->getModule('user')->logoutUrl
                            'visible' =>!Yii::app()->user->isGuest,
                        ],
                    ],
                ]); ?>
            </div><!-- mainmenu -->
            <?php if (isset($this->breadcrumbs)): ?>
                <?php $this->widget('zii.widgets.CBreadcrumbs', [
                    'links' => $this->breadcrumbs,
                ]); ?><!-- breadcrumbs -->
            <?php endif ?>

<?php echo $content; ?>

            <div class="clear"></div>

            <div id="footer">
                It's FOOTER
            </div><!-- footer -->

        </div><!-- page -->

    </body>
</html>
