<?php

class m140708_062208_group_base extends CDbMigration
{
    private $_table = '{{group}}';

    private $_data = [
                [
                    'id'     => 1,
                    'name'   => 'SU',
                ],
                [
                    'id'     => 2,
                    'name'   => 'Administrators',
                ],
                [
                    'id'     => 3,
                    'name'   => 'Chief content',
                ],
                [
                    'id'     => 4,
                    'name'   => 'Content managers',
                ],
    ];
    
    public function safeUp()
    {
	$this->createTable('{{group}}',
                [
                    'id'     => 'pk',
                    'name'   => 'string NOT NULL',
                ]
        );
        
        
        foreach ((array) $this->_data as $item) {
            
            $this->insert('{{group}}', $item);                    
        }
    }

    public function safeDown()
    {
	$this->dropTable('{{group}}');
    }
}