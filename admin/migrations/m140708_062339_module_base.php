<?php

class m140708_062339_module_base extends CDbMigration
{
    private $_table = '{{module}}';

    private $_data = [
        [
                    'id'     => 1,
                    'name'   => 'News',
        ],
        [
                    'id'     => 2,
                    'name'   => 'Product',
        ],
    ];
    
    public function safeUp()
    {
	$this->createTable('{{module}}',
                [
                    'id'     => 'pk',
                    'name'   => 'string NOT NULL',
                ]
        );
        
        foreach ((array) $this->_data as $item) {
            
            $this->insert('{{module}}', $item);                    
        }
    }

    public function safeDown()
    {
	$this->dropTable('{{module}}');
    }
}