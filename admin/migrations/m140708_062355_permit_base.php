<?php

class m140708_062355_permit_base extends CDbMigration
{
    private $_table = '{{permit}}';

    public function safeUp()
    {
	$this->createTable('{{permit}}',
                [
                    'id'        => "pk",
                    'group_id'  => 'integer(11) NOT NULL',
                    'module_id' => 'integer(11) NOT NULL',
                    'read'      => 'integer(1) NOT NULL',
                    'write'     => 'integer(1) NOT NULL',
                    'decor'     => 'integer(1) NOT NULL',
                ]
        );
    }

    public function safeDown()
    {
	$this->dropTable('{{permit}}');
    }
}