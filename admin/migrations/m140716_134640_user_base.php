<?php

/**
 * Миграция для создания таблицы предыдущей версией
 * Внимание!!! Необходимо в главном конфиге в компоненте 'db'
 *             установить префикс таблиц:  'tablePrefix' => 'ovl_',  
 */
class m140716_134640_user_base extends CDbMigration
{
    public function safeUp()
    {
        $sql =  'CREATE TABLE IF NOT EXISTS `{{user}}` (
                    `id` int(11) NOT NULL AUTO_INCREMENT,
                    `username` VARCHAR(100) NOT NULL,
                    `password` CHAR(128) NOT NULL,
                    `email` VARCHAR(128) NOT NULL,
                    `fullname` VARCHAR(100) NOT NULL,
                    `group_id` INT(11) NOT NULL,
                    `created` DATETIME NOT NULL,
                    `updated` DATETIME NOT NULL DEFAULT 0,
                    `subscribe` INT(1) NOT NULL DEFAULT 0,
                    PRIMARY KEY (`id`),
                    UNIQUE KEY (`username`)
                ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10';

        $this->execute($sql);
    }

    public function safeDown()
    {
        $this->dropTable('{{user}}');
    }
}
