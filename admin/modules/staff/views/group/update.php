<h4><?php echo CHtml::encode(Yii::t('staff', "Update Group")); ?><hr></h4>

<div class="form">

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
	'id'                   => 'staff-group-form',
        'type'                 => 'horizontal',
	'enableAjaxValidation' => false,
        'htmlOptions'          => ['class' => 'well'],
        
]); ?>

    <?php echo $form->errorSummary($modelU); ?>                
    <?php echo $form->textFieldRow($modelU, 'name'); ?>

    <?php $style = ['style' => 'text-align:center',]; ?>
    
    <?php $this->widget('bootstrap.widgets.TbGridView', [
        'type' => 'striped bordered',
        'dataProvider' => $modelP,
        'template' => "{items}",
        'columns' => [
            [
                'name'        => 'module_id',
                'header'      => 'Module', 'headerHtmlOptions' => $style,
                'value'       => '$data->module->name',
                'htmlOptions' => $style,
            ],
            [
                'name'         => 'read',
                'header'       => 'Read', 'headerHtmlOptions' => $style,
                'class'        => 'bootstrap.widgets.TbToggleColumn',
                'toggleAction' => 'group/toggleR',
                'htmlOptions'  => $style,
            ],
            [
                'name'         => 'write',
                'header'       => 'Write', 'headerHtmlOptions' => $style,
                'class'        => 'bootstrap.widgets.TbToggleColumn',
                'toggleAction' => 'group/toggleW',
                'htmlOptions'  => $style,
            ],
            [
                'name'         => 'decor',
                'header'       => 'Decor', 'headerHtmlOptions' => $style,
                'class'        => 'bootstrap.widgets.TbToggleColumn',
                'toggleAction' => 'group/toggleD',
                'htmlOptions'  => $style,
            ],
        ]
    ]); ?>

    <?php $this->widget('bootstrap.widgets.TbButton', [
                'label'       => Yii::t('staff', "Save"),
                'buttonType'  => 'submit',
                'type'        => 'success',
    ]); ?>

<?php $this->endWidget(); ?>
</div><!-- form -->    