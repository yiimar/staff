<?php
/* @var $this StaffGroupController */
/* @var $model StaffGroup */
/* @var $form TBActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
	'id'                  	 => 'staff-group-form',
        'type'                	 => 'horizontal',
        'htmlOptions'         	 => ['class' => 'well'],
	'enableClientValidation' => true,
 	'clientOptions'		 => [
  		'validateOnSubmit' => true,
  		'validateOnChange' => true, // allow client validation for every field
 	],	 
        
    ]); ?>

	<?php echo $form->errorSummary($modelC); ?>
                
        <?php echo $form->textFieldRow($modelC, 'name'); ?>


        <?php $this->widget('bootstrap.widgets.TbButton', [
                'label'       => Yii::t('staff', "Save"),
                'buttonType'  => 'submit',
                'type'        => 'success',
        ]); ?>

    <?php $this->endWidget(); ?>

</div><!-- form -->