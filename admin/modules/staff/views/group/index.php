<h4><?php echo CHtml::encode(Yii::t('staff', "Manage Staff Groups")); ?><hr></h4>

    <?php $style = ['style' => 'text-align:center',]; ?>
    
<?php $this->widget('bootstrap.widgets.TbExtendedGridView', [
        'type'          => 'striped bordered condensed',
	'id'            => 'module-grid',
	'dataProvider'  => $modelI->search(),
	'columns'       => $modelI->gridColumns(),
]); ?>

<?php Yii::app()->clientScript->registerScript('create', "
$('.create-button').click(function(){
	$('.create-form').toggle();
	return false;
});
");
?>

<?php $this->widget('bootstrap.widgets.TbButton', [
        'label'       => Yii::t('staff', "Create"),
        'type'        => 'info',
        'htmlOptions' => ['class' => 'create-button',]
]); ?>
<br><br><br><br>

<div class="create-form" style="display:none">
<h4><?php echo CHtml::encode(Yii::t('staff', "Create Group")); ?><hr></h4>    
<?php $this->renderPartial('_form', [
	'modelC' => $modelC,
        'modelT' => 'Create',
]); ?>
</div><!-- add-form -->
