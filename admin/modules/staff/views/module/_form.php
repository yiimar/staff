<!--<h4><?php // echo CHtml::encode(Yii::t('staff', $title)); ?><hr></h4>-->

<div class="form">

    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
	'id'                     => 'staff-module-form',
        'type'                   => 'horizontal',
        'htmlOptions'            => ['class' => 'well'],
        'enableClientValidation' => true,
 	'clientOptions'          => [
  		'validateOnSubmit' => true,
  		'validateOnChange' => true, // allow client validation for every field
 	],	 

    ]); ?>

	<?php echo $form->errorSummary($model1); ?>
                
            <?php echo $form->textFieldRow($model1, 'name'); ?>
    
            <?php $this->widget('bootstrap.widgets.TbButton', [
                'label'       => Yii::t('staff', $modelT),
                'buttonType'  => 'submit',
                'type'        => 'success',
            ]); ?>

    <?php $this->endWidget(); ?>
</div><!-- form -->