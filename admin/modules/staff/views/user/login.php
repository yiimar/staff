<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
    'id'   => 'horizontalForm',
    'type' => 'horizontal',
    'enableClientValidation'=>true,
    'clientOptions'         => [
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType'   => true,
    ],    
    'htmlOptions' => [
        'class' => 'well',              // for inset effect
        'id'    => 'login-form',
    ], 
]);
 
echo $form->textFieldRow($model,     'username', ['class' => 'span3']);
echo $form->passwordFieldRow($model, 'password', ['class' => 'span3']);
echo $form->checkboxRow($model,      'rememberMe');

$this->widget('bootstrap.widgets.TbButton', [
    'buttonType' => 'submit',
    'label'      => Yii::t('staff', 'Login'),
]);
 
$this->endWidget();
unset($form);
