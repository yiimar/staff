<h4><?php echo CHtml::encode(Yii::t('staff', "Update User")); ?><hr></h4>    

<?php $this->renderPartial('_form', [
    'modelF' => $modelF,
    'modelT' => 'update',
]); ?>