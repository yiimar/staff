<h4><?php echo CHtml::encode(Yii::t('staff', 'Manage User')) ?><hr></h4>

<?php $this->widget('bootstrap.widgets.TbGridView', [
	'id'            => 'staff-user-grid',
        'type'          => 'striped bordered condensed',
	'template'      => "{items}",
	'dataProvider'  => $model->search(),
	'columns'       => $model->gridColumns(),
]); ?> 

<?php Yii::app()->clientScript->registerScript('create', "
	$('.create-button').click(function(){
		$('.create-form').toggle();
		return false;
	});
"); ?>

<?php $this->widget('bootstrap.widgets.TbButton', [
        'label'       => Yii::t('staff', "Add"),
        'type'        => 'info',
        'htmlOptions' => ['class' => 'create-button',]
]); ?>

<div class="create-form" style="display:none">
    <h4><?php echo CHtml::encode(Yii::t('staff', "Create User")); ?><hr></h4>
    <?php $this->renderPartial('_form', [
	'modelF' => $modelF,
        'modelT' => "create",
    ]); ?>
</div><!-- add-form -->
