<div class="form">
    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
        'id'           		 => 'staff-user-form',
        'type'         		 => 'horizontal',
        'htmlOptions'   	 => ['class' => 'well'],
	'enableClientValidation' => true,
 	'clientOptions'		 => [
  		'validateOnSubmit' => true,
  		'validateOnChange' => true, // allow client validation for every field
 	],	 
            ]
    ); ?>
    
        <?php $create = ($modelT === 'create'); ?>

	<?php echo $form->errorSummary($modelF); ?>

        <?php
        echo $form->textFieldRow($modelF, 'username');
        
        echo $form->passwordFieldRow($modelF, 'password', [
            'class' => 'staff-user-password',
        ]);
            
        echo \CHtml::ajaxLink(
                Yii::t('staff', 'Generate'),
                ['getPassword'],
                [
                    'type'     => 'POST',
                    'dataType' => 'json',
                    'cache'    => false,
                    'success'  => "function(data) {
                        $('.staff-user-password').val(data);
                    }",
                ],
                [
                    'style' => 'margin-left: 420px;'
                ]
        ); ?>
 
        <?php if ($create) : ?>     <?php // field 'retype' for UserCreate Form  ?>
            <?php echo $form->passwordFieldRow($modelF, 'retype', ['class' => 'staff-user-password']); ?>
        <?php endif; ?>
    
        <?php
        echo $form->textFieldRow($modelF, 'fullname');
        echo $form->textFieldRow($modelF, 'email');
        
        
        echo $form->dropDownListRow($modelF, 'group_id',
                \admin\modules\staff\models\Group::getAllGroupsList(),
                ['prompt'=>Yii::t('staff', "Group selection")]
        );
           
        echo $form->checkboxRow($modelF, 'subscribe'); 
        ?>
	
        <?php $this->widget('bootstrap.widgets.TbButton',
                [
                    'label'       => $create 
                                        ? Yii::t('staff', "Create")
                                        : Yii::t('staff', "Update"),
                    'buttonType'  => 'submit',
                    'type'        => 'success',
                ]
        ); ?>
	

    <?php $this->endWidget(); ?>

</div><!-- form -->