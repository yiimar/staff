function getIndex($modelName, $model, $modelC)
{
    <h4><?php echo CHtml::encode(Yii::t('staff', "Manage " . $modelName)); ?><hr></h4>

    <?php $this->widget('bootstrap.widgets.TbGridView', [
	'id'            => 'staff-module-grid',
        'type'          => 'striped bordered condensed',
        'template'      => "{items}",
	'dataProvider'  => $model->search(),
        'columns'       => $model->gridColumns(),
    ]); ?>


    <?php Yii::app()->clientScript->registerScript('create', "
        $('.create-button').click(function(){
	    $('.create-form').toggle();
	    return false;
        });
    "); ?>
    <?php $this->widget('bootstrap.widgets.TbButton', [
        'label'       => Yii::t('staff', "Add"),
        'type'        => 'info',
        'htmlOptions' => ['class' => 'create-button',]
    ]); ?>
    <br><br><br><br>

    <div class="create-form" style="display:none">
        <h4><?php echo CHtml::encode(Yii::t('staff', "Create " . $modelName)); ?><hr></h4>    
        <?php $this->renderPartial('_form', [
	    'model1' => $modelC,
            'modelT' => 'Create',
        ]); ?>
    </div><!-- add-form -->
}
