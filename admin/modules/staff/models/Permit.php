<?php

namespace admin\modules\staff\models;

/**
 * This is the model class for table "{{permit}}".
 *
 * The followings are the available columns in table '{{permit}}':
 * @property integer $id
 * @property integer $group_id
 * @property integer $module_id
 * @property integer $read
 * @property integer $write
 * @property integer $decor
 */

use Yii;
use CDbCriteria;
use CActiveDataProvider;
use CArrayDataProvider;

class Permit extends \admin\components\ActiveRecordAbstract
{
    const STATUS_DENY  = 0;
    const STATUS_ALLOW = 1;
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Permit the static model class
     */
    public static function model($className=__CLASS__)
    {
    	return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
	return '{{permit}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
	return [
            [
                'group_id, module_id',
                'required',
            ],
            [
                'read, write, decor',
                'default',
                'value' => self::STATUS_DENY,
            ],
            [
                'group_id, module_id, read, write, decor',
                'numerical',
                'integerOnly' => true,
            ],
            
            // The following rule is used by search().
            [
                'id, group_id, module_id, read, write, decor',
                'safe',
                'on' => 'search',
            ],
	];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
	return [
            'group'  => [self::BELONGS_TO, '\\admin\\modules\\staff\\models\\Group',  'group_id'],
            'module' => [self::BELONGS_TO, '\\admin\\modules\\staff\\models\\Module', 'module_id'],
        ];
    }
    
    /**
     * Scope for data module search
     * 
     * @param type $module_id integer
     */
    public function forModule($module_id)
    {
        $this->getDbCriteria()->mergeWith([
            'condition' => "module_id={$module_id}",
        ]);
    }
    
    /**
     * Scope for data module search
     * 
     * @param type $group integer
     */
    public function forGroup($group_id)
    {
        $this->getDbCriteria()->mergeWith([
            'condition' => "group_id={$group_id}",
        ]);
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
	return [
            'id'        => 'ID',
            'group_id'  => Yii::t('staff', 'Group'),
            'module_id' => Yii::t('staff', 'Module'),
            'view'      => Yii::t('staff', 'View'),
            'edit'      => Yii::t('staff', 'Edit'),
            'decor'     => Yii::t('staff', 'Decor'),
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
       	$criteria=new CDbCriteria;

            $criteria->compare('id',        $this->id);
            $criteria->compare('group_id',  $this->group_id);
            $criteria->compare('module_id', $this->module_id);
            $criteria->compare('read',      $this->read);
            $criteria->compare('write',     $this->write);
            $criteria->compare('decor',     $this->decor);

        return new CActiveDataProvider($this, [
            'criteria'=>$criteria,
        ]);
    }
    
    public function addNewModulePermit($id)
    {
        if (!$this->count('module_id='.$id)) {
            
            $groups = \admin\modules\staff\models\Group::model()->findAll();
            
            foreach ($groups as $group) {
                $model = new self;
                    $model->group_id  = $group->id;
                    $model->module_id = $id;
                    
                if (!$model->save())
                    throw new CHttpException(404, "The row with module_id={$id} and group_id={$group->id} not saved into table {{permit}}");
            }
        }
    }
    
    public function addNewGroupPermit($id)
    {
        if (!$this->count('group_id='.$id)) {
            
            $modules = \admin\modules\staff\models\Module::model()->findAll();
            
            foreach ($modules as $module) {
                $model = new self;
                    $model->group_id  = $id;
                    $model->module_id = $module->id;
                    
                if (!$model->save())
                    throw new CHttpException(404, "The row with module_id={$module->id} and group_id={$id} not saved into table {{permit}}");
            }
        }
    }

    /**
     * Return array of base rows of this group permits
     * 
     * take group name, related permits and module name for each module
     * 
     * @param type $group_id
     * @return type array 
     */
    public function searchForGroup($id)
    {
        return new CActiveDataProvider($this, [
            'criteria' => ['condition' => 'group_id='.$id],
        ]);
    }
    
    /**
     * Return array of base rows of this module permits
     * 
     * take module name, related permits and group name for each group
     * 
     * @param type $module_id
     * @return type array 
     */
    public static function getPermitForModule($module_id)
    {
        return Yii::app()->db->createCommand()
                ->select('m.id, m.name, p.read, p.write, p.decor, g.name')
                ->from('avl_module m, avl_permit p, avl_group g')
                ->where("m.id={$module_id}
                     and p.module_id = m.id
                     and p.group_id  = g.id"
                )
                ->queryAll();
    }
}