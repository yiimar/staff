<?php

namespace admin\modules\staff\models;

/**
 * This is the model class for table "{{user}}".
 *
 * The followings are the available columns in table '{{user}}':
 * @property integer $id
 * @property string $_username
 * @property string $hash
 * @property string $email
 * @property string $name
 * @property integer $group_id
 * @property integer $subscribe
 */

use \Yii;
use \CDbCriteria;
use \CPasswordHelper;
use \CActiveDataProvider;
use \CMap;

class User extends \admin\components\ActiveRecordAbstract
{
    const SUBSCRIBE_YES = 1;
    const SUBSCRIBE_NO  = 0;
    
    private static $_user;

    public $hash;

    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className=__CLASS__)
    {
	return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
	return '{{user}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
	// NOTE: you should only define rules for those attributes that
	// will receive user inputs.CConsoleApplication
	return [
            // username rules
            [
                'username',
                'required',
            ],
            [
                'username',
                'length',
                'max' => 100, 'min' => 3,                   'message' => Yii::t('staff', "Incorrect username (length between 3 and 20 characters).")
            ],
            [
                'username',
                'match', 'pattern' => '/^[A-Za-z\.-_]+$/u', 'message' => Yii::t('staff', "Incorrect symbols (A-z).")
            ],
            [
                'username',
                'unique',                                   'message' => Yii::t('staff', "This user's name already exists.")
            ],
            // password rules
            [
                'password',
                'length',
                'max' => 64, 'min' => 4,                   'message' => Yii::t('staff', "Incorrect password (minimal length 4 symbols).")
            ],
            // email rules
            [
                'email',
                'email'
            ],
            // name rules
            [
                'fullname',
                'length',
                'max' => 100, 'min' => 3,                   'message' => Yii::t('staff', "Incorrect username (length between 3 and 20 characters).")
            ],
            // group_id, subscribe rules
            [
                'group_id',
                'in',
                'range' => array_keys(\admin\modules\staff\models\Group::getAllGroupsList())
            ],
            [
                'subscribe',
                'in',
                'range' => array_keys(self::getSubscribeList())
            ],
            // scenarios rules
            [
                'password, fullname, email, subscribe, group_id',
                'required',
                'on' => 'create',
            ],
            [
                'visited',
                'required',
                'on' => 'login'
            ],
            [
                'id, username, password, fullname, email, created, updated, group_id, subscribe',
                'safe',
                'on' => 'search'
            ],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
            'group' => [self::BELONGS_TO, '\\admin\\modules\\staff\\models\\Group', 'group_id'],
        ];
    }

    /**
     * Get user by Name:
     * 
     * @param mixed $_username - username
     * 
     * @return mixed, User instance || null
     */
    public static function byLogin($username)
    {
        return self::model()->findByAttributes(['username' => $username,]);
    }

    public static function loadUser()
    {
        if (!self::$_user instanceof User)
             self::$_user = self::model()->findByPk(Yii::app()->user->id);

        return self::$_user;
    }

    public static function createUser($data)
    {
        $id    = isset($data['id']) ? $data['id'] : null;

        $_user = self::model()->findByPk($id) ?: new self;

        $_user->setAttributes($data);

        return $_user->validate() && $_user->save();
    }

    public function scopes()
    {
        return [
            'subscribe' => ['condition' => 'subscribe=' . self::SUBSCRIBE_YES,],
        ];
    }        

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
	return [
            'id'            => Yii::t('staff', 'ID'),
            'username'      => Yii::t('staff', 'Login'),
            'password'      => Yii::t('staff', 'Password'),
            'email'         => Yii::t('staff', 'Email'),
            'fullname'      => Yii::t('staff', 'Name'),
            'group_id'      => Yii::t('staff', 'Group'),
            'subscribe'     => Yii::t('staff', 'Subscribe'),
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
	// @todo Please modify the following code to remove attributes that should not be searched.
	$criteria = new CDbCriteria;

		$criteria->compare('id',            $this->id);
//		$criteria->compare('username',      $this->username,           true);
//		$criteria->compare('password',      $this->password,        true);
//		$criteria->compare('email',         $this->email,           true);
//		$criteria->compare('name',          $this->name,            true);
//		$criteria->compare('group_id',      $this->group_id);
//		$criteria->compare('subscribe',     $this->subscribe);

	return new CActiveDataProvider($this, [
            'criteria' => $criteria,
	]);
    }

    public static function getSubscribeList()
    {
        return [
            self::SUBSCRIBE_NO  => Yii::t('staff', 'No Subscribe'),
            self::SUBSCRIBE_YES => Yii::t('staff', 'Subscribe'),
        ];
    }
    
    public function sendSubscribe()
    {
        $to = $this->email;
        $subject = Yii::t('staff', 'Congratulations! Your new profile');
        $message = "Добрый день, {$this->fullname}!.\n\n Ваши данные для входа в систему управления:\n Логин: {$this->username}, \n Пароль: {$this->password}";
        
        \admin\components\SendMail::send($to , $subject, $message);
    }

    /**
     * Generates the password
     * @param string password
     * @return string hash
     */
    public static function generatePassword($length=10)
    {
        $rnd = Yii::app()->getRequest()->getHostInfo() . date("Y-m-d H:i:s");
        
        return substr(md5(uniqid() . $rnd), 0, $length);
    }    

    /**
     * Checks if the given password is correct.
     * @param string the password to be validated
     * @return boolean whether the password is valid
     */
    public function validatePassword($password)
    {
        return CPasswordHelper::verifyPassword($password, $this->hash);
    }
    /**
     * Generates the password hash.
     * @param string password
     * @return string hash
     */
    public function hashPassword($password)
    {
        return CPasswordHelper::hashPassword($password);
    }

    public function gridColumns()
    {
        $center = ['style' => 'text-align:center',];        
        
        return [
            [
                'name' => 'username',
	        'htmlOptions'       => $center,
                'headerHtmlOptions' => $center,
            ],
            [
                'name' => 'fullname',
		'htmlOptions'       => $center,
                'headerHtmlOptions' => $center,
            ],
            [
                'name' => 'email',
                'htmlOptions'       => ['style'=>'width:100px;'],
                'headerHtmlOptions' => $center,
            ],
            [
                'name' => 'created',
                'htmlOptions' 	    => ['style'=>'width:90px;'],
                'headerHtmlOptions' => $center,
            ],
            [
                'name' => 'updated',
                'htmlOptions'       => ['style'=>'width:90px;'],
                'headerHtmlOptions' => $center,
            ],
            [
                'name'  => 'group_id',
                'value' => 'Yii::t("staff", admin\modules\staff\models\Group::getGroupName($data->group_id))',
                'htmlOptions'       => $center,
                'headerHtmlOptions' => $center,
            ],
            [
                'htmlOptions' => ['nowrap'=>'nowrap', 'style'=>'text-align:center;',],
		'class'=>'bootstrap.widgets.TbButtonColumn',

                'template'  => '{update} {delete}',
            ],
        ];
    }

    public function beforeSave()
    {
        $this->hash = $this->password;
        
        if ($this->getIsNewRecord() !== false) {        

            $this->password = $this->hashPassword($this->password);
            $this->created  = date('Y-m-d H:i:s');
            $this->updated  = date('Y-m-d H:i:s');
        
        } elseif (empty($this->password) === false) {
            
            $this->password = $this->hashPassword($this->password);
            $this->updated  = date('Y-m-d H:i:s');
        } 

        return parent::beforeSave();
    }

    public function afterSave()
    {
        if ($this->subscribe !== false) {
            $this->password = $this->hash;
            $this->sendSubscribe();
        }
        
        return parent::afterSave();
    }  
}