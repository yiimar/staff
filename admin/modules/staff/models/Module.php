<?php

namespace admin\modules\staff\models;

/**
 * This is the model class for table "{{module}}".
 *
 * The followings are the available columns in table '{{module}}':
 * @property integer $id
 * @property string $name
 */

use Yii;
use CDbCriteria;
use CActiveDataProvider;

class Module extends \admin\components\ActiveRecordAbstract
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
	return '{{module}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
	// NOTE: you should only define rules for those attributes that
	// will receive user inputs.
	return [
            [
                'name',
                'required'
            ],
            [
                'name',
                'length',
                'max' => 255
            ],
            // The following rule is used by search().
            [
                'id, name',
                'safe',
                'on' => 'search'
            ],
            [
                'name',                
                'safe',
                'on' => 'editable',
            ],
	];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
	return [
            'group'  => [self::MANY_MANY,  '\\admin\\modules\\staff\\models\\Group',  'permit(module_id, group_id)'],
        ];
    }
    
    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
	return [
            'id'     => 'ID',
            'name'   => Yii::t('staff', 'Name'),
	];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
	// @todo Please modify the following code to remove attributes that should not be searched.
	$criteria = new CDbCriteria;

            $criteria->compare('id',     $this->id);
            $criteria->compare('name',   $this->name,   true);
            
	return new CActiveDataProvider($this, [
			'criteria' => $criteria,
	]);
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Module the static model class
     */
    public static function model($className=__CLASS__)
    {
	return parent::model($className);
    }
    
    public static function getModulesArray()
    {
        return Yii::app()->db->createCommand()
                ->select('name')
                ->from('module')
                ->queryAll();                
    }
    
    public function gridColumns()
    {
        $center = ['style' => 'text-align:center',];        
        
        return [
            [
                'name'              => 'name',
                'header'            => \CHtml::encode(\Yii::t('staff' ,'Name')),
                'headerHtmlOptions' => $center,
            ],
            [
                'htmlOptions' => ['nowrap'=>'nowrap', 'style'=>'text-align:center;',],
		'class'=>'bootstrap.widgets.TbButtonColumn',

                'template'  => '{update} {delete}',
            ],
        ];
    }
    
    protected function afterDelete()
    {
        parent::afterDelete();
        
        Permit::model()->deleteAll('module_id='.$this->id);
    }
}
