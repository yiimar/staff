<?php

namespace admin\modules\staff\models;

/**
 * This is the model class for table "{{group}}".
 *
 * The followings are the available columns in table '{{group}}':
 * @property integer $id
 * @property string $name
 */

use \Yii;
use \CHtml;
use \CDbCriteria;
use \CActiveDataProvider;

class Group extends \admin\components\ActiveRecordAbstract
{
    protected static $_groupsArray;
    
    public $modelName = '\\admin\\modules\\staff\\models\\Group';

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Group the static model class
     */
    public static function model($className=__CLASS__)
    {
	return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
    	return '{{group}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
	return [
            [
                'name',
                'required',
            ],
            [
                'name',
                'length',
                'max' => 100, 'min' => 3,
                    'message' => Yii::t('user', "Incorrect username (length between 3 and 20 characters).")
            ],
            
            // The following rule is used by search().
            [
                'id, name',
                'safe',
                'on' => 'search'
            ],
	];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
	return [
            'user'   => [self::HAS_MANY,  '\\admin\\modules\\staff\\models\\User',   'group_id'],
            'count'  => [self::STAT,      '\\admin\\modules\\staff\\models\\User',   'group_id'],
            'module' => [self::MANY_MANY, '\\admin\\modules\\staff\\models\\Module', 'permit(group_id, module_id)'],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
    	return [
            'id'   => 'ID',
            'name' => Yii::t('staff', 'Name'),
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
	$criteria = new CDbCriteria;

            $criteria->compare('id',   $this->id);
            $criteria->compare('name', $this->name, true);

	return new CActiveDataProvider($this, [
            'criteria' => $criteria,
        ]);
    }

    /**
     * Return employee name by id
     * 
     * @param type $pk
     * @return type string
     */
    public static function getGroupName($pk)
    {
        $group = self::model()->findByPk($pk);
        
        return $group->name;
    }

    protected static function getGroupsArray()
    {
        if (self::$_groupsArray === null)
            self::$_groupsArray = self::model()->findAll();
        
        return self::$_groupsArray;
    }

    /**
     * Return array of user groups
     * 
     * @return type array
     */
    public static function getAllGroupsList()
    {
        return CHtml::listData(self::getGroupsArray(), 'id', 'name');
    }

    public function gridColumns()
    {
        $center = ['style' => 'text-align:center',];        
        
            return [
            [
                'name'              => 'name',
                'headerHtmlOptions' => $center,
                'htmlOptions'       => $center,
            ],
            [
                'name'              => CHtml::encode(Yii::t("staff", "Users")),
                'headerHtmlOptions' => $center,
                'value'             => '$data->count',
                'htmlOptions'       => $center,
            ],
            [
                'htmlOptions' => ['nowrap'=>'nowrap', 'style'=>'text-align:center;',],
		'class'=>'bootstrap.widgets.TbButtonColumn',

                'template'  => '{update} {delete}',
            ],
        ];
    }
    
    protected function afterDelete()
    {
        parent::afterDelete();
        
        Permit::model()->deleteAll('group_id='.$this->id);
    }
}