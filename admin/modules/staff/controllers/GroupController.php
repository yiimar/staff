<?php

namespace admin\modules\staff\controllers;

use \Yii;

/**
 * Class GroupController
 * 
 * yiimar, 06/2014
 */
class GroupController extends \admin\components\AdminController
{
    public $modelClass  = '\\admin\\modules\\staff\\models\\Group';
    public $permitClass = '\\admin\\modules\\staff\\models\\Permit';

    /**
     * @return array action filters
     */
    public function filters()
    {
	return [
//			'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
	];
    }
    
    /**
     * Actions for TbExtendedGridView toggle colunm
     * 
     * @return type array
     */
    public function actions()
    {
        return [
            'toggleR' => [
                'class'     => 'bootstrap.actions.TbToggleAction',
                'modelName' => $this->permitClass,
            ],
            'toggleW' => [
                'class'     => 'bootstrap.actions.TbToggleAction',
                'modelName' => $this->permitClass,
            ],
            'toggleD' => [
                'class'     => 'bootstrap.actions.TbToggleAction',
                'modelName' => $this->permitClass,
            ],
        ];
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
//	public function accessRules()
//	{
//		return array(
//			array('allow', // allow authenticated user to perform 'create' and 'update' actions
//				'actions'=>array('create','update','delete'),
//				'users'=>array('admin'),
//			),
//			array('deny',  // deny all users
//				'users'=>array('*'),
//			),
//		);
//	}

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->render('index', [
            'modelI' => new $this->modelClass('search'),
            'modelC' => new \admin\modules\staff\forms\GroupCreateForm,
        ]);
    }

    /**
     * Updates a particular model with related permits for modules access.
     * 
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->render('update', [
            'modelU'  => new \admin\modules\staff\forms\GroupUpdateForm,
            'modelP' => \CActiveRecord::model($this->permitClass)->searchForGroup($id),
        ]);
    }

    /**
     * Deletes a particular model with related permits for modules access.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
	$this->loadModel($id)->delete();

        \CActiveRecord::model($this->permitClass)->forGroup($id)->delete();

	// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (Yii::app()->getRequest()->getIsAjaxRequest())       Yii::app()->end(200, true);
        else                                                    $this->getController()->redirect(['index']);
    }
}