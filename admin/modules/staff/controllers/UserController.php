<?php

namespace admin\modules\staff\controllers;

use \Yii;

use admin\modules\staff\models\User;

class UserController extends \admin\components\AdminController
{
    public $modelClass  = '\\admin\\modules\\staff\\models\\User';
    
    public function filters()
    {
        return [
            'ajaxOnly + getPassword',
            'postOnly + delete', // we only allow deletion via POST request
        ];
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->render('index', [
            'model'  => new $this->modelClass('search'),
            'modelF' => new \admin\modules\staff\forms\UserCreateForm,
        ]);
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->render('update', ['modelF' => new \admin\modules\staff\forms\UserUpdateForm($id)]);
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
	$this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (Yii::app()->getRequest()->getIsAjaxRequest())       Yii::app()->end(200, true);
        else                                                    $this->getController()->redirect(['index']);
    }
    
    public function actionLogin()
    {
        $this->render('login', ['model' => new \admin\modules\staff\forms\LoginForm]);
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout()
    {
	Yii::app()->user->logout();
        
	$this->redirect(Yii::app()->homeUrl);
    }
    
    public function actionGetPassword()
    {
        echo \CJSON::encode(User::generatePassword());
    }
}
