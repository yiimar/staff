<?php

namespace admin\modules\staff\controllers;

use \Yii;
use CActiveForm;
use CHttpException;

use admin\modules\staff\models\Module;

/**
 * Class ModuleController
 * 
 * yiimar, 06/2014
 */
class ModuleController extends \admin\components\AdminController
{
    public $modelClass  = '\\admin\\modules\\staff\\models\\Module';
    public $permitClass = '\\admin\\modules\\staff\\models\\Permit';
    
    /**
     * @return array action filters
     */
    public function filters()
    {
    	return [
//			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
        ];
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
	return [
            [
                'allow',  // allow all users to perform 'index' and 'view' actions
		'actions' => ['index','update'],
                'users'   => ['@'],
            ],
            [
                'allow', // allow admin user to perform 'admin' and 'delete' actions
		'actions' => ['delete'],
		'users'   => ['admin'],
            ],
            [
                'deny',  // deny all users
		'uers' => ['*'],
            ],
	];
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->render('index', [
            'model'  => new $this->modelClass('search'),
            'modelC' => new \admin\modules\staff\forms\ModuleCreateForm,
        ]);
    }

    public function actionUpdate()
    {
        $this->render('update', ['modelU' => new \admin\modules\staff\forms\ModuleUpdateForm]);
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
	$this->loadModel($id)->delete();

        \CActiveRecord::model($this->permitClass)->forModule($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (Yii::app()->getRequest()->getIsAjaxRequest())       Yii::app()->end(200, true);
        else                                                    $this->getController()->redirect(['index']);
    }
}