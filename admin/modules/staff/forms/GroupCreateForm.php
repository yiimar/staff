<?php

namespace admin\modules\staff\forms;

/**
 * Description of ModuleCreateForm class
 */

use Yii;
use admin\modules\staff\models\Group;

class GroupCreateForm extends \admin\components\FormModel
{
    public $id;
    public $name;
    
    public function init()
    {
        $data = $this->populate();
        
        if ($data !== null) {
            if ($this->validate()) {
                
                $model = new Group();
                
                $model->attributes = $data;
            
                if ($model->save()) {
                    \admin\modules\staff\models\Permit::model()->addNewGroupPermit($model->id);
                    
                    Yii::app()->controller->redirect(['/staff/group/index']);;
                }                

            }                
        }
        return $this;            
    }
 
    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules()
    {
        return [
            // name rules
            [
                'name',
                'length',
                'max' => 100, 'min' => 3,                   'message' => Yii::t('staff', "Incorrect username (length between 3 and 20 characters).")
            ],
            [
                'name',
                'match', 'pattern' => '/^[A-Za-z0-9\.-_]+$/u',
		'message' => Yii::t('staff', 'Incorrect symbols (A-z, 0-9, "." ).'),
            ],
            [
                'name', 'unique',                           'message' => Yii::t('staff', 'Sorry, this name has already been taken.'),
                    'allowEmpty'    => false,
                    'attributeName' => 'name',
                    'caseSensitive' => false,
                    'className'     => '\\admin\\modules\\staff\\models\\Group',
            ],

            // scenarios rules
            [
                'name',
                'required',
            ],
        ];
    }
}