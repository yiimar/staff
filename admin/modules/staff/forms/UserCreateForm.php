<?php

namespace admin\modules\staff\forms;

/**
 * Description of ModuleCreateForm class
 */

use \Yii;

class UserCreateForm extends \admin\components\FormModel
{
    public $modelClass  = '\\admin\\modules\\staff\\models\\User';

    /**
     * Models attributes
     */
    public $id;
    public $username;
    public $password;
    public $retype;
    public $fullname;
    public $email;
    public $group_id;
    public $subscribe = \admin\modules\staff\models\User::SUBSCRIBE_NO;
   
    public function init()
    {
        $data = $this->populate();
        
        if ($data !== null) {
            if ($this->validate()) {
                $model = new $this->modelClass('create');                
            
                unset($data['retype']);
                $model->attributes = $data;
            
                if ($model->save())
                    Yii::app()->controller->redirect(['index']);
            }                
        }
        return $this;
    }
 
    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules()
    {
	return [
            [
                'username, password, retype',
                'filter',
                'filter' => 'trim',                
            ],
            // login rules
            [
                'username',
                'length',
                'max' => 100, 'min' => 3,                   'message' => Yii::t('staff', "Incorrect username (length between 3 and 20 characters)."),
            ],
            [
                'username',
                'match', 'pattern' => '/^[A-Za-z0-9\.-_]+$/u',
		'message' => Yii::t('staff', 'Incorrect symbols (A-z, "." ).'),
            ],
            [
                'username', 'unique',                       'message' => Yii::t('staff', 'Sorry, this username has already been taken.'),
                    'allowEmpty'    => false,
                    'attributeName' => 'username',
                    'caseSensitive' => false,
                    'className'     => $this->modelClass,
            ],
            // password, retype rules
            [
                'password',
                'length',
                'max' => 64, 'min' => 4,                    'message' => Yii::t('staff', "Incorrect password (minimal length 4 symbols)."),
            ],
            [
                'retype',
                'compare',
                'compareAttribute' => 'password',           'message' => Yii::t('staff', "Incorrect password (minimal length 4 symbols)."),
                
            ],
            // email rules
            [
                'email',
                'email'
            ],
            // name rules
            [
                'fullname',
                'length',
                'max' => 100, 'min' => 3,                   'message' => Yii::t('staff', "Incorrect username (length between 3 and 20 characters)."),
            ],
            // group_id, subscribe rules
            [
                'group_id',
                'in',
                'range' => array_keys(\admin\modules\staff\models\Group::getAllGroupsList())
            ],
            [
                'subscribe',
                'in',
                'range' => array_keys(\admin\modules\staff\models\User::getSubscribeList())
            ],
            // scenarios rules
            [
                'username, password, fullname, email, retype, group_id',
                'required',
            ],
	];
        
    }
}