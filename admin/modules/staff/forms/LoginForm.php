<?php
/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
namespace admin\modules\staff\forms;

use Yii;
use admin\modules\staff\components\UserIdentity;

class LoginForm extends \admin\components\FormModel
{
    public $username;
    public $password;
    public $rememberMe = false;

    private $_identity;

    /**
     * Form init:
     * 
     * @return void
     */
    public function init()
    {        
        // AJAX validation:
        $this->ajaxValidate('login-form');
        // Fill form and try to login:        // Validate model && login
        if (($this->populate() !== null) && ($this->validate() && $this->login()))
            // Redirect to index page:
            Yii::app()->controller->redirect(
                    Yii::app()->user->returnUrl
            );
    }

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules()
    {
        return [
            ['username, password',  'required'],         // username and password are required
            ['rememberMe',          'boolean'],          // rememberMe needs to be a boolean
            ['password',            'authenticate'],     // password needs to be authenticated
        ];
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return [
            'username'   => Yii::t('app', 'Login'),
            'password'   => Yii::t('app', 'Password'),
            'rememberMe' => Yii::t('app', 'Remember Me'),
        ];
    }

    /**
     * Authenticates the password.
     * This is the 'authenticate' validator as declared in rules().
     *
     * @param string $attribute - attribute
     * @param mixed  $params    - params
     * 
     * @return void
     */
    public function authenticate($attribute, $params)
    {
        if ($this->hasErrors() === false) {
            
            $this->_identity = new UserIdentity($this->username, $this->password);
            
            if(!$this->_identity->authenticate())
                $this->addError('password', Yii::t('staff', 'Incorrect username or password.'));
        }
    }

    /**
     * Logs in the user using the given username and password in the model.
     * @return boolean whether login is successful
     */
    public function login()
    {
        if ($this->_identity === null) {
            
            $this->_identity = new UserIdentity($this->username, $this->password);            
            $this->_identity->authenticate();
        }

        if ($this->_identity->errorCode === UserIdentity::ERROR_NONE) {
            $duration = $this->rememberMe
                        ? 3600 * 24 * 30 // 30 days
                        : 0; // None
            
            Yii::app()->user->login($this->_identity, $duration);
            
            $user = \admin\modules\staff\models\User::byLogin($this->username);
                $user->setScenario('login');                
                $user->visited = date('Y-m-d H:i:s');
            $user->save();
            
            return true;        
        }

        return false;
    }
}