<?php

namespace admin\modules\staff\forms;

/**
 * Description of UserUpdateForm class
 */

use \Yii;

class UserUpdateForm extends \admin\modules\staff\components\UpdateFormModel
{
    public $id;
    public $username;
    public $password;
    public $fullname;
    public $email;
    public $group_id;
    public $subscribe = false;
    
    protected $_redirect  = '/staff/user/index';
    protected $_baseModel = "admin\modules\staff\models\User";
    
    
    public function getClassType()
    {
        return str_replace([__NAMESPACE__, NS, 'User', 'Form'], "", get_class($this));
    }
    
    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules()
    {
	return [
            [
                'username, password',
                'filter',
                'filter' => 'trim',                
            ],
            // login rules
            [
                'username',
                'length',
                'max' => 100, 'min' => 3,                   'message' => Yii::t('user', "Incorrect username (length between 3 and 20 characters).")
            ],
            [
                'username',
                'match', 'pattern' => '/^[A-Za-z\.-_]+$/u',     'message' => Yii::t('user', "Incorrect symbols (A-z).")
            ],
            // password,  rules
            [
                'password',
                'length',
                'max' => 64, 'min' => 4,                    'message' => Yii::t('user', "Incorrect password (minimal length 4 symbols).")
            ],
            // email rules
            [
                'email',
                'email'
            ],
            // name rules
            [
                'fullname',
                'length',
                'max' => 100, 'min' => 3,                   'message' => Yii::t('user', "Incorrect username (length between 3 and 20 characters).")
            ],
            // group, subscribe rules
            [
                'group_id',
                'in',
                'range' => array_keys(\admin\modules\staff\models\Group::getAllGroupsList())
            ],
            [
                'subscribe',
                'in',
                'range' => array_keys(\admin\modules\staff\models\User::getSubscribeList())
            ],
            // scenarios rules
	];        
    }
}
