<?php

namespace admin\modules\staff;

use \Yii;

class StaffModule extends \CWebModule
{
    public $routes;    
    public $controllerNamespace = '\admin\modules\staff\controllers';    
    public $defaultController   = 'user';
}
