<?php

namespace admin\modules\staff\components;

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
use CWebUser;
use admin\modules\staff\models\User;

class UserIdentity extends \CUserIdentity
{
    private $_id;
 
    public function authenticate()
    {
//        $username = strtolower($this->username);
        $user     = User::byLogin($this->username);
        
        if($user === null)                                          $this->errorCode = self::ERROR_USERNAME_INVALID;
        else if(!$user->validatePassword($this->password))          $this->errorCode = self::ERROR_PASSWORD_INVALID;
        else {
            $this->_id      = $user->id;
            $this->username = $user->username;
            
            $this->setState('group', $user->group);
            $this->setState('data',  $user->getAttributes());            
            
            $this->errorCode= self::ERROR_NONE;
        }
        
        return !$this->errorCode;
    }
 
    public function getId()
    {
        return $this->_id;
    }
}