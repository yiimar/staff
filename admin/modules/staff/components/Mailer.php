<?php

namespace admin\modules\staff\components;

use Yii;

class Mailer
{  
    public static function mailsend($to,  $toName="", $subject, $message, $from = "", $fromName = 'NOTUS') 
    {
        
        $mail = Yii::app()->mailer;
        $mail->CharSet = "utf-8";
        $mail->SetFrom($from, $fromName);
        $mail->Subject = $subject;
        $mail->MsgHTML($message);
        $mail->AddAddress($to, $toName);
        
        return $mail->Send()
                ? "Message sent!"
                : "Mailer Error: " . $mail->ErrorInfo;
    }
}