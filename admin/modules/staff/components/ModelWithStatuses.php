<?php

namespace admin\modules\staff\components;

use Yii;

class ModelWithStatuses extends \admin\components\ActiveRecordAbstract
{
    const STATUS_ACTIVE   = 0;
    const STATUS_INACTIVE = 1;
    const STATUS_ARCHIVE  = 2;
    
    public function scopes()
    {
        return [
            'active' => [
                'condition' => 'status=' . self::STATUS_ACTIVE,
            ],
            'notactive' => [
                'condition' => 'status=' . self::STATUS_INACTIVE,
            ],
            'banned' => [
                'condition' => 'status=' . self::STATUS_ARCHIVE,
            ],
        ];
    }

    /**
     * Available stasuses array
     * 
     * @return type array
     */
    public static function getStatusList()
    {
        return [
            self::STATUS_ACTIVE   => \Yii::t('staff', 'Active'),
            self::STATUS_INACTIVE => \Yii::t('staff', 'Inactive'),
            self::STATUS_ARCHIVE  => \Yii::t('staff', 'Archive'),
        ];
    }

    /**
     * Rerurn name of status
     * 
     * @param type $key
     * @return type string
     */
    public static function getStatusByKey($key)
    {
        $ar = self::getStatusList();
        
        return $ar[$key];
    }
}
