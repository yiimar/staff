<?php

namespace admin\modules\staff\components;

/**
 * Description of Model class
 *
 * @category Component
 * @package  admin.modules.staff.components
 */

use \Yii;
use \CActiveForm;
use \CHttpException;

abstract class UpdateFormModel extends \CFormModel
{
    /**
     * Redirect url after save
     * @var type string
     */
    protected $_redirect;
    /**
     * String of the base model class name
     * @var type string
     */
    protected $_baseModel;

    /**
     * @var CActiveRecord the currently loaded data model instance.
     */
    public $_model;
    
    public function init()
    {
        $model = $this->loadModel();
        
        $data = $this->populate();
        
        if (($data !== null) && ($this->validate())) {
            
                $model->attributes = $data;
            
                if ($model->save())
                    Yii::app()->controller->redirect([$this->_redirect]);
        }        
        
        $this->setAttributes($model->getAttributes());        
        
        return $this;
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     */
    public function loadModel()
    {        
    	if($this->_model===null) {
            
            $pk = Yii::app()->request->getQuery('id');
            
            $baseModel = new $this->_baseModel;
            $this->_model = $baseModel::model()->findbyPk($pk);
            
            if($this->_model===null)
                throw new CHttpException(404,'The requested page does not exist.');
        }
 	
        return $this->_model; 
    }

    /**
     * Fill form fields:
     * 
     * @return mixed, request data
     */
    public function populate()
    {
        $class = str_replace('\\', '_', get_class($this));

        $data = Yii::app()->getRequest()->getPost($class);

        $this->setAttributes($data);

        return $data;
    }

    /**
     * AJAX validation:
     * 
     * @param string $ajaxValue - request value name
     * 
     * @return void
     */
    public function ajaxValidate($ajaxValue = null)
    {
        if (Yii::app()->getRequest()->getPost('ajax') === $ajaxValue) {
            echo CActiveForm::validate($this);
            
            Yii::app()->end();
        }
    }

    /**
     * Get placeholder:
     * 
     * @param string $field - field name
     * 
     * @return string, field placeholder
     */
    public function getPlaceHolder($field)
    {
        return Yii::t('staff', 'Input :field_name', [
            ':field_name' => mb_strtolower($this->getAttributeLabel($field), 'UTF-8'),
        ]);
    }    
}