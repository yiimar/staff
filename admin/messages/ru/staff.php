<?php

return [
    'Create Employee'                                   => 'Добавление нового пользователя',
    'Update Employee'                                   => 'Редактирование пользователя',
    'List of Employees'                                 => 'Список пользователей',
    'Manage Employees'                                  => 'Управление пользователями',
    'Name'                                              => 'Имя',
    'Status'                                            => 'Статус',
    'Login'                                             => 'Логин',
    'Password'                                          => 'Пароль',
    'Group'                                             => 'Группа',
    'Email'                                             => 'Email',
    'Administrators'                                    => 'Администраторы',
    'Add'                                               => 'Добавить',
    'Create'                                            => 'Создать',
    'Save'                                              => 'Сохранить',
    'Generate'                                          => 'Сгенерировать',
    'Active'                                            => 'Активно',
    'Inactive'                                          => 'Деактивировано',
    'Archive'                                           => 'В архиве',
    
    
    
];
