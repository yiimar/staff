<?php
/**
 * index.php
 *
 * @author: yiimar
 * Date: 7/22/14
 * Time: 11:13 AM
 */

$dir = dirname(__FILE__);
        
require_once $dir . '/config/definitions.php';

$yiic   = VENDOR_PATH  . '/yiisoft/yii/framework/yiic.php';
$config = $dir . '/config/console.php';

require_once($yiic);
